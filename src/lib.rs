use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use std::path::{Path, PathBuf};

mod schema;
mod grammar;
mod deser;
mod loader;
mod file_loader;

pub use grammar::Block;
pub use loader::Loader;
pub use file_loader::FileLoader;
pub use schema::{Schema, Kind};

pub type StdError = Box<dyn std::error::Error + Sync + Send>;
pub type StdResult<T=()> = Result<T, StdError>;

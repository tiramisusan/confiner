use super::*;

pub(super) trait Root<Out>
{
    fn handle(
        &self,
        block:    &Block,
        compiler: &mut Compiler<'_, Out>
    )
        -> StdResult;
}

pub(super) fn new_root<T, Out>(
    cb: impl Fn(&mut Out, Arc<T>, &Block) -> StdResult + 'static
)
    -> Box<dyn Root<Out>>
    where T: Any, Out: Any
{
    Box::new(RootImpl { cb: Box::new(cb) })
}

struct RootImpl<T, Out>
{
    cb: Box<dyn Fn(&mut Out, Arc<T>, &Block) -> StdResult>
}

impl<T, Out> Root<Out> for RootImpl<T, Out>
    where T: 'static, Out: 'static
{
    fn handle(
        &self,
        block:    &Block,
        compiler: &mut Compiler<'_, Out>
    )
        -> StdResult
    {
        let strong = compiler.create::<T>(block)?
            .upgrade()
            .expect("Expected compilation to be completed");

        compiler.apply_to_output(move |out| (self.cb)(out, strong, block))
    }
}


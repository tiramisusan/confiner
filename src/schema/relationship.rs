use super::*;

pub(super) trait Relationship<Out>
{
    fn handle(
        &self,
        parent:   &mut dyn Any,
        block:    &Block,
        compiler: &mut Compiler<'_, Out>
    )
        -> StdResult;
}

pub(super) fn new_relationship<A, B, Out>(
    cb: impl Fn(&mut A, Weak<B>, &Block) -> StdResult + 'static
)
    -> Box<dyn Relationship<Out> + 'static>
    where A: Any, B: Any, Out: 'static
{
    Box::new(RelationshipImpl { cb: Box::new(cb) })
}

struct RelationshipImpl<A, B>
{
    cb: Box<dyn Fn(&mut A, Weak<B>, &Block) -> StdResult + 'static>
}

impl<A, B, Out> Relationship<Out> for RelationshipImpl<A, B>
    where A: Any, B: Any, Out: 'static
{
    fn handle(
        &self,
        parent:   &mut dyn Any,
        block:    &Block,
        compiler: &mut Compiler<'_, Out>
    )
        -> StdResult
    {
        let parent_down = parent.downcast_mut::<A>()
            .expect("Parent has unexpected type");

        let child = compiler.create::<B>(block)?;

        (self.cb)(parent_down, child, block)
    }
}

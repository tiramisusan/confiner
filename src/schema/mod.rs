use super::*;

use std::any::{Any, TypeId};
use std::sync::{Arc, Weak};

mod output;
use output::*;

mod root;
use root::*;

mod creator;
use creator::*;

mod relationship;
use relationship::*;

mod compiler;
use compiler::*;

type RelationshipKey = (Option<String>, TypeId, TypeId);

#[derive(Default)]
pub struct Schema<Out>
{
    types:         HashMap<String,          TypeId>,
    creators:      HashMap<TypeId,          Box<dyn Creator<Out>>>,
    roots:         HashMap<TypeId,          Box<dyn Root<Out>>>,
    relationships: HashMap<RelationshipKey, Box<dyn Relationship<Out>>>
}

pub trait Kind: for<'b> serde::Deserialize<'b> + Any
{
    fn kind_name() -> &'static str;

    fn create(block: &Block) -> StdResult<Self>
    {
        Ok(<Self as serde::Deserialize>::deserialize(block.as_ref())?)
    }
}

impl<Out> Schema<Out>
    where Out: 'static
{
    fn get_type(&self, block: &Block) -> StdResult<TypeId>
    {
        self.types.get(&block.kind)
            .cloned()
            .ok_or(format!("Unknown block type: {:?}", block.kind).into())
    }

    fn handle_create(&self, block: &Block, compiler: &mut Compiler<'_, Out>)
        -> StdResult
    {
        let type_id = self.get_type(block)?;
        if let Some(creator) = self.creators.get(&type_id)
        {
            creator.create(block, compiler)
        }
        else
        {
            Err(format!("No creator for kind {:?}", block.kind).into())
        }
    }

    fn handle_root(
        &self,
        block:    &Block,
        compiler: &mut Compiler<'_, Out>
    )
        -> StdResult
    {
        let type_id = self.get_type(block)?;
        if let Some(root) = self.roots.get(&type_id)
        {
            root.handle(block, compiler)
        }
        else
        {
            Err(format!("Kind {:?} is not a valid root", block.kind).into())
        }
    }

    fn handle_relationship(
        &self,
        parent:   &mut dyn Any,
        role:     Option<&str>,
        block:    &Block,
        compiler: &mut Compiler<'_, Out>
    )
        -> StdResult
    {
        let a_type_id = <dyn Any>::type_id(parent);
        let b_type_id = self.get_type(block)?;
        let key = &(role.map(str::to_owned), a_type_id, b_type_id);

        if let Some(relationship) = self.relationships.get(&key)
        {
            relationship.handle(parent, block, compiler)
        }
        else
        {
            Err(format!("Kind {:?} is not a valid child", block.kind).into())
        }
    }

    pub fn load(&self, loader: &Loader) -> StdResult<Output<Out>>
        where Out: Default
    {
        self.load_with(loader, Default::default())
    }

    pub fn load_with(&self, loader: &Loader, out: Out) -> StdResult<Output<Out>>
    {
        Compiler::new(loader, self, out).compile()
    }

    fn add_kind<T>(&mut self) -> TypeId
        where T: Kind
    {
        let type_id = TypeId::of::<T>();
        if !self.creators.contains_key(&type_id)
        {
            self.types.insert(T::kind_name().to_string(), type_id);
            self.creators.insert(type_id, new_creator::<T, Out>());
        }

        type_id
    }

    pub fn add_root<T>(
        &mut self,
        cb: impl Fn(&mut Out, Arc<T>, &Block) -> StdResult + 'static
    )
        where T: Kind
    {
        let type_id = self.add_kind::<T>();
        self.roots.insert(type_id, new_root(cb));
    }

    fn add_relationship_inner<A, B>(
        &mut self,
        role: Option<&str>,
        cb:   impl Fn(&mut A, Weak<B>, &Block) -> StdResult + 'static
    )
        where A: Kind, B: Kind
    {
        let a_type_id = self.add_kind::<A>();
        let b_type_id = self.add_kind::<B>();
        let key = (role.map(str::to_owned), a_type_id, b_type_id);

        self.relationships.insert(key, new_relationship(cb));
    }

    pub fn add_relationship<A, B>(
        &mut self,
        cb:  impl Fn(&mut A, Weak<B>, &Block) -> StdResult + 'static
    )
        where A: Kind, B: Kind
    {
        self.add_relationship_inner::<A, B>(None, cb)
    }

    pub fn add_relationship_with_role<A, B>(
        &mut self,
        role: &str,
        cb:   impl Fn(&mut A, Weak<B>, &Block) -> StdResult + 'static
    )
        where A: Kind, B: Kind
    {
        self.add_relationship_inner::<A, B>(Some(role), cb)
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    fn load_with_schema<Out>(s: Schema<Out>, conf: &str) -> Output<Out>
        where Out: Default + 'static
    {
        let mut loader = Loader::default();
        loader.add_document(conf, ".").unwrap();
        s.load(&loader).unwrap()
    }

    #[test]
    fn test_schema()
    {
        #[derive(Default, PartialEq, Debug)]
        struct Root { a: Vec<Arc<A>>, b: Vec<Arc<B>> }

        #[derive(serde::Deserialize, PartialEq, Debug)]
        struct A { a: i32 }
        #[derive(serde::Deserialize, PartialEq, Debug)]
        struct B { b: i32 }

        impl Kind for A { fn kind_name() -> &'static str { "a" } }
        impl Kind for B { fn kind_name() -> &'static str { "b" } }

        let mut schema = Schema::<Root>::default();
        schema.add_root::<A>(|root, a, _| { root.a.push(a); Ok(()) });
        schema.add_root::<B>(|root, b, _| { root.b.push(b); Ok(()) });

        assert_eq!(
            load_with_schema::<Root>(schema, ":a { a=1 } :b { b=2 }").as_ref(),
            &Root
            {
                a: vec![ Arc::new(A { a: 1 }) ],
                b: vec![ Arc::new(B { b: 2 }) ]
            }
        )
    }

    #[test]
    fn test_schema_relationship()
    {
        #[derive(serde::Deserialize)]
        struct A { #[serde(skip)] children: Vec<Weak<B>> }
        #[derive(serde::Deserialize)]
        struct B { b: i32 }

        impl Kind for A { fn kind_name() -> &'static str { "a" } }
        impl Kind for B { fn kind_name() -> &'static str { "b" } }

        let mut schema = Schema::<Vec<Arc<A>>>::default();

        schema.add_root::<A>(|root, a, _| { root.push(a); Ok(()) });
        schema.add_relationship::<A, B>(
            |a, b, _| { a.children.push(b); Ok(()) }
        );

        let out = load_with_schema(schema, ":a { :b { b=100 } }");
        assert_eq!(out.len(), 1);
        assert_eq!(out[0].children.len(), 1);
        assert_eq!(out[0].children[0].upgrade().unwrap().b, 100);
    }

    #[test]
    fn test_schema_circular_relationship()
    {
        #[derive(serde::Deserialize)]
        struct A { #[serde(skip)] child: Vec<Weak<A>>, a: i32 }

        impl Kind for A { fn kind_name() -> &'static str { "a" } }

        let mut schema = Schema::<Vec<Arc<A>>>::default();

        schema.add_root::<A>(|root, a, _| { root.push(a); Ok(()) });
        schema.add_relationship::<A, A>(
            |a1, a2, _| { a1.child.push(a2); Ok(()) }
        );

        let out = load_with_schema(schema, "&l:a { &l, a=2 } :a { &l, a=1 }");

        assert_eq!(out.len(), 1);

        assert_eq!(out[0].a, 1);
        assert_eq!(out[0].child.len(), 1);
        let child = out[0].child[0].upgrade().unwrap();

        assert_eq!(child.a, 2);
        assert_eq!(child.child.len(), 1);
        let grandchild = child.child[0].upgrade().unwrap();

        assert_eq!(grandchild.a, 2);
        assert_eq!(grandchild.child.len(), 1);
    }
}

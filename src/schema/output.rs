use super::*;

pub struct Output<Out>
{
    _records: HashMap<usize, Record>,
    out:      Out
}

impl<Out> std::ops::Deref for Output<Out>
{
    type Target = Out;

    fn deref(&self) -> &Out { self.as_ref() }
}

impl<Out> std::ops::DerefMut for Output<Out>
{
    fn deref_mut(&mut self) -> &mut Out { self.as_mut() }
}

impl<Out> Output<Out>
{
    pub(super) fn new(_records: HashMap<usize, Record>, out: Out) -> Self
    {
        Self { _records, out }
    }

    pub fn as_ref(&self) -> &Out { &self.out }
    pub fn as_mut(&mut self) -> &mut Out { &mut self.out }
}

use super::*;

pub(super) trait Creator<Out>
{
    fn create(&self, block: &Block, compiler: &mut Compiler<'_, Out>)
        -> StdResult;
}

pub(super) fn new_creator<T, Out>() -> Box<dyn Creator<Out> + 'static>
    where T: Kind, Out: 'static
{
    Box::new(CreatorImpl::<T> { _spooky: Default::default() })
}

struct CreatorImpl<T>
{
    _spooky: std::marker::PhantomData<T>
}

impl<T, Out> Creator<Out> for CreatorImpl<T>
    where T: Kind, Out: 'static
{
    fn create(
        &self,
        block:    &Block,
        compiler: &mut Compiler<'_, Out>
    )
        -> StdResult
    {
        let mut deser = T::create(block)?;
        let mut result = Ok(());
        let new = Arc::new_cyclic(
            |this|
            {
                compiler.begin(block, this.clone());
                result = compiler.compile_children(&mut deser, block);
                deser
            }
        );
        compiler.complete(block, new);

        result
    }
}

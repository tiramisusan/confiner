use super::*;

pub(super) enum Record
{
    Incomplete(Box<dyn Any> /* Contains Weak<T> */),
    Complete(Box<dyn Any> /* Contains Arc<T> */)
}

pub(super) struct Compiler<'a, Out>
{
    records: HashMap<usize, Record>,
    loader:  &'a Loader,
    schema:  &'a Schema<Out>,
    out:     Out
}

fn to_id(b: &Block) -> usize { Arc::as_ptr(b) as usize }

impl<'a, Out> Compiler<'a, Out>
    where Out: 'static
{
    pub(super) fn new(
        loader: &'a Loader,
        schema: &'a Schema<Out>,
        out:    Out
    )
        -> Self
    {
        Self { records: Default::default(), loader, schema, out }
    }

    pub(super) fn create<T>(&mut self, block: &Block) -> StdResult<Weak<T>>
        where Weak<T>: Any
    {
        let id = to_id(block);

        if !self.records.contains_key(&id)
        {
            self.schema.handle_create(block, self)?;
        }

        match self.records.get(&id)
            .expect("Expected block to have been created")
        {
            Record::Incomplete(weak) =>
                Ok(
                    weak.downcast_ref::<Weak<T>>()
                        .expect("Block has wrong type")
                        .clone()
                ),
            Record::Complete(strong) =>
                Ok(
                    Arc::downgrade(
                        &strong.downcast_ref::<Arc<T>>()
                            .expect("Block has wrong type")
                            .clone()
                    )
                )
        }
    }

    pub(super) fn begin<T>(&mut self, b: &Block, weak: Weak<T>)
        where Weak<T>: Any
    {
        self.records.insert(to_id(b), Record::Incomplete(Box::new(weak)));
    }

    pub(super) fn complete<T>(&mut self, b: &Block, strong: Arc<T>)
        where Arc<T>: Any
    {
        self.records.insert(to_id(b), Record::Complete(Box::new(strong)));
    }

    pub(super) fn compile(mut self) -> StdResult<Output<Out>>
    {
        for block in self.loader.roots()
        {
            self.schema.handle_root(&block, &mut self)?;
        }

        Ok(Output::new(self.records, self.out))
    }

    pub(super) fn compile_children(
        &mut self,
        parent:       &mut dyn Any,
        parent_block: &Block,
    )
        -> StdResult
    {
        for (role, child) in self.loader.children_and_role(parent_block)?
        {
            self.schema.handle_relationship(parent, role, &child, self)?;
        }

        Ok(())
    }

    pub(super) fn apply_to_output(
        &mut self,
        cb: impl FnOnce(&mut Out) -> StdResult
    )
        -> StdResult
    {
        (cb)(&mut self.out)
    }
}
